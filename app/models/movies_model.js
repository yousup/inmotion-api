const dbConnection = require("../../db");

// Movie object constructor
function Movie(movie) {
	this.actors = movie.actors;
	this.title = movie.title;
	this.year = movie.year;
	this.rating = movie.rating;
	this.genre = movie.genre;
}

Movie.getAllMovies = (result) => {
	dbConnection.query("Select id, title, year, rating, genre, actors from Movies", (err, res) => {
		if(err) {
			console.log("error: ", err);
			result(null, err);
		}
		else{
			console.log('movies : ', res); 
			result(null, res);
		}
	})
}

Movie.getMovieImage = (id, result) => {
	dbConnection.query("Select image from Movies where id = ?", id, (err, res) => {
		if(err) {
			console.log("error: ", err);
			result(null, err);
		}
		else{
			result(null, res);
		}
	})
}

Movie.createMovie = (newMovie, result) => {
	dbConnection.query("Insert into Movies set ?", newMovie, (err, res) => {
		if(err) {
			console.log("error: ", err);
			result(err, null);
		}
		else{
			console.log("here");
			result(null, res.insertId);
		}
	})
}

Movie.imageUpload = (image, id, result) => {
	dbConnection.query("Update Movies set image = ? where id = ?", [image, id], (err, res) => {
		if(err) {
			console.log("error: ", err);
			result(err, null);
		}
		else{
			// console.log("here");
			result(null, res.insertId);
		}
	})
}

Movie.deleteMovie = (id, result) => {
	dbConnection.query("delete from Movies where id = ?", id, (err, res) => {
		if(err) {
			console.log("error: ", err);
			result(null, err);
		}
		else{
			result(null, res);
		}
	})
}

Movie.updateMovie = (id, movie, result) => {
	dbConnection.query("update Movies set ? where id = ?", [movie, id], (err, res) => {
		if(err) {
			console.log("error: ", err);
			result(null, err);
		}
		else{
			result(null, res);
		}
	})
}

module.exports = Movie;
const moviesController = require("../controllers/movies_controller");

// CRUD routes for movies
module.exports = (app) => {
	app.get('/', (req, res) => {
		moviesController.getAllMovies(req, res);
	})
	app.post('/create', (req, res) => {
		moviesController.createMovie(req, res);
	})
	app.post('/imageUpload/:id', (req, res) => {
		moviesController.imageUpload(req, res, req.params.id);
	})
	app.delete('/delete/:id', (req, res) => {
		moviesController.deleteMovie(req, res, req.params.id);
	})
	app.patch('/update/:id', (req, res) => {
		moviesController.updateMovie(req, res, req.params.id);
	})
	app.get('/image/:id', (req, res) => {
		moviesController.getMovieImage(req, res, req.params.id);
	})
};
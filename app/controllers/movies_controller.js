const Movie = require("../models/movies_model");

// Movies controller

exports.getAllMovies = (req, res) => {
	Movie.getAllMovies((err, movies) => {
		if (err) {
			console.log(err);
			res.json({error: err});
		}
		else {
			console.log(movies);
			res.json({movies: movies});
		}
	});
}

exports.createMovie = (req, res) => {
	let movie = new Movie(req.body);
	Movie.createMovie(movie, (err, movieId) => {
		if (err) {
			res.json({
				status: "failed",
				statusMessage: "There was an error in creating the movie",
				error: err
			});
		}
		else {
			res.json({
				id: movieId,
				status: "success",
				statusMessage: "Movie created successfully!"
			})
		}
	})
}

exports.imageUpload = (req, res, id) => {
	Movie.imageUpload(req.files.file.data, id, (err, movieId) => {
		if (err) {
			res.json({
				status: "failed",
				statusMessage: "There was an error in uploading the image",
				error: err
			});
		}
		else {
			res.json({
				id: movieId,
				status: "success",
				statusMessage: "Image uploaded successfully!"
			})
		}
	})
}

exports.deleteMovie = (req, res, id) => {
	console.log("id: "+ id)
	Movie.deleteMovie(id, (err, response) => {
		if (err) res.json({error: err});
		else {
			console.log(response);
			res.json(response);
		}
	})
}

exports.updateMovie = (req, res, id) => {
	let movie = new Movie(req.body);
	Movie.updateMovie(id, movie, (err, response) => {
		if (err) {
			res.json({
				status: "failed",
				statusMessage: "There was an error in creating the movie",
				error: err
			});
		}
		else {
			res.json({
				id: id,
				status: "success",
				statusMessage: "Movie updated successfully!"
			})
		}
	})
}

exports.getMovieImage = (req, res, id) => {
	Movie.getMovieImage(id, (err, image) => {
		if (err) {
			console.log(err);
			res.json({error: err});
		}
		else {
			console.log(image);
			res.json({image: image});
		}
	});
}
const express = require('express');
const bodyParser = require('body-parser');
var cors = require('cors');
const fileUpload = require('express-fileupload');
const port = 8080;

const app = express();

// Enables body decoding, if the req is sent with encoding
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(fileUpload());

// enable cors
app.use(cors());

// This brings in any defined routes
require('./app/routes')(app);

// MySQL connection
const connection = require("./db");
connection.connect(function(err) {
  if (err) {
    console.error('error connecting: ' + err.stack);
    return;
  }
  console.log('connected as id ' + connection.threadId);
});

// app.use(function (req, res, next) {
// 	res.header("Access-Control-Allow-Origin", "*");
// 	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
// 	next();
// });

app.listen(port, () => {
	console.log('App is live at port ' + port);
})




const mysql = require('mysql');

const connection = mysql.createConnection({
    host     : 'yousuplee-mysql.c1uvnjaqfltq.us-east-2.rds.amazonaws.com',
    user     : 'username',
    password : 'password',
    database : 'MoviesDB'
});

// connection.connect(function(err) {
//     if (err) throw err;
// });

module.exports = connection;